#include <mono/jit/jit.h>
#include <mono/metadata/assembly.h>

#include <iostream>
#include <string>
#include <fstream>

// Global variables, for example purposes
static MonoDomain* rootDomain = nullptr;
static MonoDomain* appDomain = nullptr;

static char* ReadBytes(const std::string& filepath, uint32_t* outSize)
{
	std::ifstream stream(filepath, std::ios::binary | std::ios::ate);

	if (!stream)
	{
		// Failed to open the file
		return nullptr;
	}

	std::streampos end = stream.tellg();
	stream.seekg(0, std::ios::beg);
	uint32_t size = end - stream.tellg();

	if (size == 0)
	{
		// File is empty
		return nullptr;
	}

	char* buffer = new char[size];
	stream.read((char*)buffer, size);
	stream.close();

	*outSize = size;
	return buffer;
}

static MonoAssembly* LoadCSharpAssembly(const std::string& assemblyPath)
{
	uint32_t fileSize = 0;
	char* fileData = ReadBytes(assemblyPath, &fileSize);

	// NOTE: We can't use this image for anything other than loading the assembly because this image doesn't have a reference to the assembly
	MonoImageOpenStatus status;
	MonoImage* image = mono_image_open_from_data_full(fileData, fileSize, 1, &status, 0);

	if (status != MONO_IMAGE_OK)
	{
		const char* errorMessage = mono_image_strerror(status);
		// Log some error message using the errorMessage data
		return nullptr;
	}

	MonoAssembly* assembly = mono_assembly_load_from_full(image, assemblyPath.c_str(), &status, 0);
	mono_image_close(image);

	// Don't forget to free the file data
	delete[] fileData;

	return assembly;
}

static MonoClass* GetClassInAssembly(MonoAssembly* assembly, const char* namespaceName, const char* className)
{
	MonoImage* image = mono_assembly_get_image(assembly);
	MonoClass* klass = mono_class_from_name(image, namespaceName, className);

	if (klass == nullptr)
	{
		// Log error here
		return nullptr;
	}

	return klass;
}

static MonoObject* InstantiateClass(MonoAssembly* assembly, const char* namespaceName, const char* className)
{
	// Get a reference to the class we want to instantiate
	MonoClass* testingClass = GetClassInAssembly(assembly, "", "CSharpTesting");

	// Allocate an instance of our class
	MonoObject* classInstance = mono_object_new(appDomain, testingClass);

	if (classInstance == nullptr)
	{
		// Log error here and abort
	}

	// Call the parameterless (default) constructor
	mono_runtime_object_init(classInstance);

	return classInstance;
}
