# Test Mono

Study Mono Runtime

```sh
# Navigate to project directory
cd /path/to/project/root/

# Build with CMake, the build folder will be created automatically
cmake -S . -B build
```

## References

- [Mono Embedding for Game Engines](https://nilssondev.com/mono-guide/book/introduction.html)
- [How to embed C# scripting into your C++ application](https://medium.com/@lewiscomstive/how-to-embed-c-scripting-into-your-c-application-782b2e57245a)
