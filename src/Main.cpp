#include <mono/jit/jit.h>
#include <mono/metadata/assembly.h>

#include "./CSLoader.cpp"

// Create mono runtime, only needed to be called once during the lifetime of your app
bool InitialiseMono()
{
	// You can choose what directory to load the core library .dll files from (mscorlib, System, System.Core)
	//mono_set_assemblies_path("./4.0/");

	// Initialise mono runtime
	rootDomain = mono_jit_init("YourRuntimeName");
	if (!rootDomain) return false; // Failed to initialise, log & return

	// Created successfully
	return true;
}

void CreateAppDomain()
{
	// Create your app domain, this is what stores your loaded assemblies in memory.
	//  Second parameter is optional configuration file path
	//char* domain = "YourCustomAppDomainName";
	appDomain = mono_domain_create_appdomain(nullptr, nullptr);

	// Tell mono to set this domain as the default one.
	//   Second parameter forces this domain to be used
	mono_domain_set(appDomain, true);
}

void CallPrintFloatVarMethod(MonoObject* objectInstance)
{
	// Get the MonoClass pointer from the instance
	MonoClass* instanceClass = mono_object_get_class(objectInstance);

	// Get a reference to the method in the class
	MonoMethod* method = mono_class_get_method_from_name(instanceClass, "PrintFloatVar", 0);

	if (method == nullptr)
	{
		// No method called "PrintFloatVar" with 0 parameters in the class, log error or something
		return;
	}

	// Call the C# method on the objectInstance instance, and get any potential exceptions
	MonoObject* exception = nullptr;
	mono_runtime_invoke(method, objectInstance, nullptr, &exception);

	// TODO: Handle the exception
}

void CallCSharp()
{
	const char* path = "D:/_workspace/_lang/cpp/Mono/CSharpTester/CSharpTester/bin/Debug/netstandard2.0/CSharpTester.dll";

	MonoAssembly* assembly = LoadCSharpAssembly(path);

	MonoObject* testInstance = InstantiateClass(assembly, "", "CSharpTesting");
	CallPrintFloatVarMethod(testInstance);
}

int main()
{
	// Setup scripting environment
	if (!InitialiseMono())
		return -1;

	CreateAppDomain();

	CallCSharp();

	// Clean up scripting
	mono_jit_cleanup(rootDomain);
	rootDomain = nullptr;
	appDomain = nullptr;

	return 0;
}
